const base = require("./../base");
const Tplmsg = require("./../../../../../lib/wxa/tplmsg.class");

module.exports = class extends base {
    async _initialize() {
        await super.isWxaAuth();
    }

    async _before() {
        this.tplMsg = new Tplmsg(
            process.env.OPEN_APPID,
            process.env.OPEN_APPSECRET
        );
        try {
            this.state.wxa = await this.checkWxaAuthorizerAccessToken(
                this.state.wxa
            );
        } catch (err) {
            this.status = 500;
            throw err;
        }
    }

    /**
     *
     * @api {get} /home/wxa/tplmsg/msgTplList 获取模板消息列表
     * @apiDescription 获取模板消息列表
     * @apiGroup App Home
     * @apiVersion 0.0.1
     *
     * @apiHeader {String} WxaToken 小程序授权token.
     *
     * @apiParam {Number} offset 偏移
     * @apiParam {Number} count 数量
     *
     * @apiSuccess {Array} success 模版消息列表
     *
     * @apiSampleRequest /home/wxa/tplmsg/msgTplList
     *
     */
    async msgTplList() {
        const { offset = 0, count = 10 } = this.query;
        const msgtpl = await this.tplMsg.get_tplmsg_list(
            this.state.wxa.authorizer_access_token,
            offset,
            count
        );
        if (msgtpl.errmsg === "ok") {
            this.success(msgtpl);
        } else {
            this.fail(this.tplMsgErr(msgtpl));
        }
    }

    /**
     *
     * @api {get} /home/wxa/tplmsg/keywordLib 获取模板库某个模板标题下关键词库
     * @apiDescription 获取模板库某个模板标题下关键词库
     * @apiGroup App Home
     * @apiVersion 0.0.1
     *
     * @apiHeader {String} WxaToken 小程序授权token.
     *
     * @apiParam {String} tpl_title_id tpl_title_id
     *
     * @apiSuccess {Array} success 模版消息列表
     *
     * @apiSampleRequest /home/wxa/tplmsg/keywordLib
     *
     */
    async keywordLib() {
        const { tpl_title_id = "" } = this.query;
        const keyword = await this.tplMsg.get_keyword_lib(
            this.state.wxa.authorizer_access_token,
            tpl_title_id
        );
        if (keyword.errmsg === "ok") {
            this.success(keyword);
        } else {
            this.fail(this.tplMsgErr(keyword));
        }
    }

    /**
     *
     * @api {get} /home/wxa/tplmsg/addTplMsg 添加至帐号下的个人模板库
     * @apiDescription 添加至帐号下的个人模板库
     * @apiGroup App Home
     * @apiVersion 0.0.1
     *
     * @apiHeader {String} WxaToken 小程序授权token.
     *
     * @apiParam {String} tpl_title_id tpl_title_id
     * @apiParam {String} keyword_id_list keyword_id_list
     *
     * @apiSuccess {Object} success 添加模版消息结果
     *
     * @apiSampleRequest /home/wxa/tplmsg/addTplMsg
     *
     */
    async addTplMsg() {
        let result = "";
        const { tpl_title_id, keyword_id_list } = this.query;
        const tpl = await this.model("wxa_tplmsg")
            .query({
                where: {
                    wxa_id: this.state.wxa.id,
                    title_id: tpl_title_id,
                    keyword_id: keyword_id_list
                }
            })
            .fetch();
        if (!tpl) {
            result = await this.tplMsg.add_tplmsg(
                this.state.wxa.authorizer_access_token,
                tpl_title_id,
                keyword_id_list
            );

            if (result.errmsg === "ok") {
                const wxaTplmsg = await this.model("wxa_tplmsg")
                    .forge({
                        wxa_id: this.state.wxa.id,
                        title_id: tpl_title_id,
                        keyword_id: keyword_id_list,
                        tpl_id: result.template_id
                    })
                    .save();
                this.success(wxaTplmsg, "添加成功");
            } else {
                this.fail(this.tplMsgErr(result));
            }
        } else {
            this.success(tpl, "已存在");
        }
    }

    /**
     *
     * @api {get} /home/wxa/tplmsg/delTplMsg 删除帐号下的某个模板
     * @apiDescription 删除帐号下的某个模板
     * @apiGroup App Home
     * @apiVersion 0.0.1
     *
     * @apiHeader {String} WxaToken 小程序授权token.
     *
     * @apiParam {Number} tpl_id tpl_id
     *
     * @apiSuccess {Object} success 删除模版消息结果
     *
     * @apiSampleRequest /home/wxa/tplmsg/delTplMsg
     *
     */
    async delTplMsg() {
        const { tpl_id } = this.query;
        const result = await this.tplMsg.del_tplmsg(
            this.state.wxa.authorizer_access_token,
            tpl_id
        );
        if (result.errmsg === "ok") {
            this.success("删除成功");
        } else {
            this.fail(this.tplMsgErr(result));
        }
    }

    /**
     *
     * @api {get} /home/wxa/tplmsg/myTplMsg 获取帐号下已存在的模板列表
     * @apiDescription 获取帐号下已存在的模板列表
     * @apiGroup App Home
     * @apiVersion 0.0.1
     *
     * @apiHeader {String} WxaToken 小程序授权token.
     *
     * @apiParam {Number} offset 偏移
     * @apiParam {Number} count 数量
     *
     * @apiSuccess {Object} success 我的模版消息结果
     *
     * @apiSampleRequest /home/wxa/tplmsg/myTplMsg
     *
     */
    async myTplMsg() {
        const { offset = 0, count = 10 } = this.query;
        const tplmsg = await this.tplMsg.get_mytplmsg_list(
            this.state.wxa.authorizer_access_token,
            offset,
            count
        );

        if (tplmsg.errmsg === "ok") {
            this.success(tplmsg);
        } else {
            this.fail(this.tplMsgErr(tplmsg));
        }
    }

    /**
     *
     * @api {post} /home/wxa/tplmsg/sendTplMsg 发送模版消息
     * @apiDescription 发送模版消息
     * @apiGroup App Home
     * @apiVersion 0.0.1
     *
     * @apiHeader {String} WxaToken 小程序授权token.
     *
     * @apiParam {String} openid openid
     * @apiParam {Number} tpl_id tpl_id
     * @apiParam {Number} form_id form_id
     * @apiParam {String} data 数据
     * @apiParam {String} page 点击模版消息跳转
     * @apiParam {String} keyword 关键词
     *
     * @apiSuccess {Object} success 发送模版消息结果
     *
     * @apiSampleRequest /home/wxa/tplmsg/sendTplMsg
     *
     */
    async sendTplMsg() {
        const {
            openid,
            tpl_id,
            form_id,
            data = "",
            page = "",
            keyword = ""
        } = this.post;

        const result = await this.tplMsg.send_tplmsg(
            this.state.wxa.authorizer_access_token,
            openid,
            tpl_id,
            form_id,
            data,
            page,
            keyword
        );
        console.log("模版消息结果", result);
        if (result.errmsg === "ok") {
            this.success("发送成功");
        } else {
            this.fail(this.tplMsgErr(result));
        }
    }
};
