const base = require("./base");

module.exports = class extends base {
    async _initialize() {
        await super.isCustomAuth();
    }

    /**
     *
     * @api {get} /home/custom/update 更新客户信息
     * @apiDescription 更新客户信息
     * @apiGroup App Home
     * @apiVersion 0.0.1
     *
     * @apiHeader {String} Token 用户登录授权token.
     *
     * @apiParam {String} nickname 昵称
     * @apiParam {String} qq qq
     * @apiParam {Number} sex 性别
     * @apiParam {String} signature 签名
     * @apiParam {String} avater_id 头像
     *
     * @apiSuccess {Object} success 客户信息
     *
     * @apiSampleRequest /home/custom/update
     *
     */
    async update() {
        const { nickname, qq, sex, signature, avater_url } = this.post;
        const customId = this.state.custom.id;

        const custom = await this.model("custom")
            .forge({
                id: customId,
                nickname,
                sex,
                qq,
                signature,
                avater_url
            })
            .save();

        this.success(custom);
    }
};
