const moment = require("moment");
const base = require("./base");

module.exports = class extends base {
    async _initialize() {
        await super.isCustomAuth();
    }

    /**
     *
     * @api {post} /shop/home/app/create 创建应用
     * @apiDescription 创建应用
     * @apiGroup Shop Home
     * @apiVersion 0.0.1
     *
     * @apiHeader {String} Token 用户授权token.
     *
     * @apiParam {String} name 名称
     * @apiParam {String} info 简介
     * @apiParam {Number} template_industry_id 行业id
     * @apiParam {String} province 省
     * @apiParam {String} city 市
     * @apiParam {String} district 区
     *
     * @apiSampleRequest /shop/home/app/create
     *
     */
    async create() {
        const {
            name,
            info,
            template_industry_id = 0,
            province,
            city,
            district
        } = this.post;
        const customId = this.state.custom.id;

        const started_at = moment().format("YYYY-MM-DD HH:mm:ss");
        const ended_at = moment()
            .add(1, "years")
            .format("YYYY-MM-DD HH:mm:ss");

        const app = await this.model("app")
            .forge({
                custom_id: customId,
                template_id: 1, // 零售
                template_industry_id: template_industry_id, // 应用分类id
                name: name, // 应用名称
                info: info, // 应用简介
                started_at: started_at, // 开始时间
                ended_at: ended_at // 结束时间
            })
            .save();

        const shop = await this.model("shop")
            .forge({
                app_id: app.id,
                name,
                info,
                status: 1
            })
            .save();

        // 添加地址信息
        await this.model("app_address")
            .forge({
                app_id: app.id,
                province: province, // 省
                city: city, // 市
                district: district // 区
            })
            .save();

        this.success(app, "应用创建成功");
    }

    /**
     *
     * @api {get} /shop/home/app/templateWxa 获取应用模版小程序主题
     * @apiDescription 获取应用模版小程序主题
     * @apiGroup Shop Home
     * @apiVersion 0.0.1
     *
     * @apiHeader {String} Token 用户授权token.
     *
     * @apiSampleRequest /shop/home/app/templateWxa
     *
     */
    async templateWxa() {
        const templateWxa = await this.model("template_wxa")
            .query(qb => {
                qb.where("template_id", "=", 2);
                qb.orderBy("rank", "desc");
            })
            .fetchAll();

        this.success(templateWxa);
    }

    /**
     *
     * @api {get} /shop/home/app/templateIndustry 获取应用模版行业
     * @apiDescription 获取应用模版行业
     * @apiGroup Shop Home
     * @apiVersion 0.0.1
     *
     * @apiHeader {String} Token 用户授权token.
     *
     * @apiSampleRequest /shop/home/app/templateIndustry
     *
     */
    async templateIndustry() {
        const templateIndustry = await this.model("template_industry")
            .query(qb => {
                qb.where("template_id", "=", 2);
            })
            .fetchAll();
        this.success(templateIndustry);
    }

    async _before_checkWxa() {
        await super.isAppAuth();
    }

    /**
     *
     * @api {get} /shop/home/app/checkWxa 检查当前应用下是否授权小程序
     * @apiDescription 检查当前应用下是否授权小程序
     * @apiGroup Shop Home
     * @apiVersion 0.0.1
     *
     * @apiHeader {String} Token 用户授权token.
     * @apiHeader {String} AppToken 应用授权token.
     *
     * @apiSampleRequest /shop/home/app/checkWxa
     *
     */
    async checkWxa() {
        const appId = this.state.app.id;
        const wxa = await this.model("wxa")
            .query(qb => {
                qb.where("app_id", "=", appId);
                qb.where("authorized", "=", 1);
                qb.where("status", "=", 1);
            })
            .fetch();
        if (wxa) {
            this.success();
        } else {
            this.fail();
        }
    }
};
